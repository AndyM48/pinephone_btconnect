# Pinephone BTConnect

A Bluetooth utility for the Pinephone and other Linux mobile devices

## Requirements

The only dependencies are tcl, tk, bluetoothctl (bluez/bluez-utils), expect, rfkill and sudo.

## Operations

- Check that bluetooth is started
- List known bluetooth devices
- Connect a bluetooth device
- Scan for and pair bluetooth devices
- Edit devices to trust, block, disconnect and remove devices.

## Screenshots

![List](screenshots/list.png) ![Edit](screenshots/edit.png)

## Installaton

Download the source code and run install.sh to:

	Copy the pinephone_btconnect programme to /usr/bin

	Copy the icons to /usr/share/pixmaps/pinephone_btconnect

	Copy the desktop file to /usr/share/applications


To update: download the source code and run install.sh again.

To uninstall Pinephone_btconnect run uninstall.sh from the downloads directory.

This is the first release of the software. If you find it useful, good. If you do not then please file a bug report.
