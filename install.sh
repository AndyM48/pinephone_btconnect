#! /bin/sh

sudo cp pinephone_btconnect /usr/bin
sudo chmod 0755 /usr/bin/pinephone_btconnect
sudo mkdir -p /usr/share/pixmaps/pinephone_btconnect
sudo cp icons/* /usr/share/pixmaps/pinephone_btconnect
sudo cp pinephone_btconnect.desktop /usr/share/applications

echo -e "\nInstall done\n"
